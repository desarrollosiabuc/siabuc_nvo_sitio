<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = ['producto', 'costo_mxn','costo_mxn_desc', 'costo_usd', 'costo_usd_desc', 'descripcion'];
}
