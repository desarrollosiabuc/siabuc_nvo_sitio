<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PreCotizacion extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    //public $solicitante;
    public $cotizacion;

    public function __construct(array $cotizacion)
    {
        //$this->solicitante = $solicitante;
        $this->cotizacion = $cotizacion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = trans('cadenas.ventas_mail_subject_precotiza');
        return $this->view('correo')
                    ->with($this->cotizacion)
                    ->subject($subject);
    }
}
