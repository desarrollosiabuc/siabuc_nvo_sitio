<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactaVentas extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $nombre;
    public $apellido;
    public $email;
    public $telefono;
    public $mensaje;

    public function __construct(Request $request)
    {
        $this->nombre = $request->input('nombre');
        $this->apellido = $request->input('apellido');
        $this->email = $request->input('email');
        $this->telefono = $request->input('telefono');
        $this->mensaje = $request->input('mensaje');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = trans('cadenas.ventas_mail_subject_contacta');
        return $this->view('correoc')
                    ->subject($subject);
    }
}
