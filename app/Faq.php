<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = ['faq_esp_mx', 'faq_eng_usa','r_esp_mx', 'r_eng_usa'];
}
