<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Cotizacion;
use App\DetalleCotizacion;
use App\Mail\ContactaVentas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;


class ContactaController extends Controller
{
    public function index(Request $request)
    {
        $correo = trans('cadenas.ventas_cotizador_remitente');
        $cc = $request->input('email');
        Mail::to($correo)
            ->cc($cc)
            ->send(new ContactaVentas ($request));
        return null;
    }
}