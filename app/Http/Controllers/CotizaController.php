<?php

namespace App\Http\Controllers;

use App\Producto;
use App\Cotizacion;
use App\DetalleCotizacion;
use App\Mail\PreCotizacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;


class CotizaController extends Controller
{
    public function calcula_costos(Request $request, $idCotizacion)
    {
        $pais = $request->input('pais');
        $inputs = $request->all();
        $costos = array();
        $costo_total = 0;

        foreach ($inputs as $key => $value) {
            if (preg_match('/chec_/', $key))
            {
                $detalleCotizacion = new DetalleCotizacion;
                $id = preg_split('/_/', $key) [1];
                if ($pais == 'MX')
                {
                    $costo = Producto::find($id)->costo_mxn;
                }
                else
                {
                    $costo = Producto::find($id)->costo_usd;
                }
                $producto = Producto::find($id)->producto;
                array_push ($costos, array('dato' => $producto,
                                           'valor' => $costo,));
                $costo_total += $costo;
                $detalleCotizacion->idCotizacion = $idCotizacion;
                $detalleCotizacion->idProducto = $id;
                $detalleCotizacion->save();
            }
        }
        array_push ($costos, array('dato' => 'costo_total',
                                   'valor' => $costo_total,));

        return $costos;
    }

    public function index(Request $request)
    {
        $nombre = $request->input('nombre');
        $correo = $request->input('correo');
        $institucion = $request->input('institucion');
        $cargo = $request->input('cargo');
        $correo_env = $request->input('envia_mail');
        $pais = $request->input('pais');

        $cotizacion = new Cotizacion;
        $cotizacion->nombre = $nombre;
        $cotizacion->email = $correo;
        $cotizacion->institucion = $institucion;
        $cotizacion->cargo = $cargo;
        $cotizacion->pais = $pais;
        $cotizacion->save();

        $idCotizacion = $cotizacion->id;

        $cotizacion = $this->calcula_costos ($request, $idCotizacion);
        $solicitante = $request->all();
        $cc = trans('cadenas.ventas_cotizador_remitente');
        
        if ($correo_env)
        {
            Mail::to($correo)
                ->cc($cc)
                ->send(new PreCotizacion ($cotizacion));
        }


        return View::make('partials.resultado_cotiza')->with('cotizacion', $cotizacion);
    }
}