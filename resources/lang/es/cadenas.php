<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    /*Header textos de error 404, 500 - 505*/
    '404_contenido_header' => 'Error 404',
    '404_contenido_header_desc' => 'La p&aacute;gina que buscas no existe. Intenta ir a otra secci&oacute;n del sitio desde el men&uacute; superior.',

    '500_contenido_header' => 'Error 500',
    '500_contenido_header_desc' => 'Error del servidor, intenta nuevamente.',

    '501_contenido_header' => 'Error 501',
    '501_contenido_header_desc' => 'Error del servidor, intenta nuevamente.',

    '502_contenido_header' => 'Error 502',
    '502_contenido_header_desc' => 'Error del servidor, intenta nuevamente.',

    '503_contenido_header' => 'Error 503',
    '503_contenido_header_desc' => 'El servidor se encuentra en mantenimiento, intenta regresar m&aacute;s tarde.',

    '504_contenido_header' => 'Error 504',
    '504_contenido_header_desc' => 'Error del servidor, intenta nuevamente.',

    '505_contenido_header' => 'Error 505',
    '505_contenido_header_desc' => 'Error del servidor, intenta nuevamente.',

    /*Cadenas del correo*/
    'precotizacion_correo_saludo' => 'Estimado cliente,',
    'precotizacion_correo_intro' => 'Agradecemos su inter&eacute;s por nuestro software de automatizaci&oacute;n para bibliotecas, y de acuerdo a su solicitud le hacemos llegar la siguiente cotizaci&oacute;n:',
    'precotizacion_correo_cc_header' => 'Aclaraciones:',
    'precotizacion_correo_cc_1' => 'El presente correo solo representa una estimaci&oacute;n del costo del software.',
    'precotizacion_correo_cc_2' => 'En la lista anterior se reflejan los costos unitarios de cad auno de los art&iacute;culos enumerados.',
    'precotizacion_correo_cc_3' => 'Los costos que se muestran est&aacute; expresados en pesos mexicanos para los clientes dentro del territorio mexicano, en cualquier otro caso los precios se expresan en dolares estadounidenses.',
    'precotizacion_correo_cc_4' => 'Para adquirir el software es necesario contactar con nuestro departamento de comercializaci&oacute;n.',


    /*Barra de Navegaci&oacute;n*/
    'nav_titulo' => 'SIABUC',
    'nav_inicio' => 'INICIO',
    'nav_nosotros' => 'NOSOTROS',
    'nav_soporte' => 'SOPORTE',
    'nav_comercializacion' => 'COMERCIALIZACI&Oacute;N',
    'nav_documentacion' => 'DOCUMENTACI&Oacute;N',
    'nav_alternar_navegacion' => 'Alternar navegaci&oacute;n',
    'nav_bradcrumbs_home' => 'Inicio',
    
    /*Header P&aacute;gina de Inicio*/
    'inicio_header_titulo' => 'SIABUC',
    'inicio_header_titulo_sub' => 'CATALOGA &bullet; INDEXA &bullet; PRESTA',
    'inicio_header_mensaje' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores .',
    'inicio_header_conozca_mas' => 'CONOZCA M&Aacute;S',
    'inicio_header_comprar_siabuc' => 'ADQUIERA SIABUC',

    /*Contacto general TODOS LADOS*/
    'contactanos_msg' => 'Si desea contactar a nuestro departamento de comercializaci&oacute;n... estamos a su disposici&oacute;n.',
    'contactanos_btn' => 'Contacte Ahora',

    /*Errores de Formularios*/
    'form_error_vacio' => 'Este campo no puede quedar vac&iacute;o.',
    'form_error_vacio_select' => 'Este campo no puede quedar vac&iacute;o, debe elejir un valor',
    'form_error_email_equivocado' => '&Eacute;ste no es un email v&aacute;lido',

    
    /*Footer*/
    'footer_ucol' => 'Universidad de Colima',
    'footer_dgti' => 'Direcci&oacute;n General de Tecnolog&iacute;as dela Informaci&oacute;n',
    'footer_dpto' => 'Desarrollo de SIABUC',
    'footer_domicilio' => 'Direcci&oacute;n',
    'footer_domicilio_2' => 'Av. Gonz&aacute;lo de Sandoval # 444 :: Colonia Las V&iacute;boras :: C.P. 28040 :: Colima, Colima, M&eacute;xico',
    'footer_tel' => 'Tel&eacute;fono',
    'footer_tel_2' => '+52 (312) 316-1121',
    'footer_correo' => 'Correo',
    'footer_correo_2' => 'siabuc@ucol.mx',
    'footer_redes_sociales' => 'REDES SOCIALES',
    'footer_derechos' => '&copy; 2016 Derechos Reservados :: Departamento de Desarrollo de SIABUC',

    /*Cadenas solo del contenido de inicio*/
    'inicio_contenido_saludo' => 'BIENVENIDO',
    'inicio_contenido_saludo_1' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse porta urna felis. Morbi eleifend aliquam lorem sed venenatis. Mauris et aliquet ex, in molestie ipsum. Vivamus sit amet suscipit libero.',
    'inicio_contenido_saludo_2' => 'Proin pharetra vehicula tincidunt.',
    'inicio_contenido_saludo_3' => 'Nam ut pulvinar orci. Ut rutrum orci non ullamcorper dictum. In nisi turpis, convallis ut purus dapibus, facilisis mollis metus. Nulla lectus est, egestas a consequat a, feugiat eu ante. Interdum et malesuada fames ac ante ipsum primis in faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Fusce tincidunt libero eu varius ullamcorper. Vestibulum bibendum risus a nibh lobortis egestas.',
    'inicio_contenido_saludo_4' => 'Donec id rhoncus orci, nec mollis enim. Duis ex purus, fringilla quis sem ut, pellentesque ullamcorper ipsum. Praesent augue nisl, maximus et dolor vitae, pharetra ornare tellus. In hac habitasse platea dictumst. Fusce arcu eros, placerat et metus nec, tincidunt pellentesque orci. Sed euismod justo lectus, vel luctus lorem tristique ac. Fusce vehicula ligula luctus lacus placerat, sed fermentum justo suscipit. Vestibulum lacus odio, cursus ut commodo ut, sodales ut eros. Morbi non nunc quis urna faucibus eleifend sed et neque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nullam ac auctor nibh.',
    'inicio_contenido_es_usuario' => 'Si ya eres usuario SIABUC',
    'inicio_contenido_actualizaciones' => 'ACTUALIZA SIABUC 9',
    'inicio_contenido_actualizaciones_2' => 'Nulla eget dictum ante, non rhoncus augue. Sed fermentum nisl sit amet consequat bibendum. Nam et velit nec ex imperdiet imperdiet id sed mi. Vestibulum vehicula justo in erat fringilla, nec viverra erat laoreet. Donec suscipit iaculis velit.',
    'inicio_contenido_poliza' => 'ACTIVA/RENUEVA TU POLIZA',
    'inicio_contenido_poliza_2' => 'Suspendisse ultrices, augue sed accumsan lacinia, libero diam semper libero, id tincidunt sapien tortor ultrices nisi. Maecenas et sem egestas, malesuada dui id, ultricies diam.',
    'inicio_contenido_ticket' => 'CREAR TICKET DE SOPORTE',
    'inicio_contenido_ticket_2' => 'Nullam a risus tristique, tempus nibh congue, pulvinar velit. Vestibulum porta, lorem sit amet ultricies pulvinar, nibh felis pretium risus, eget scelerisque nisl enim et ex.',
    'inicio_contenido_casos_exito' => 'CASOS DE &Eacute;XITO',

    /*Cadenas del contenido de la secci&oacute;n NOSOTROS*/

    'nosotros_contenido_header' => 'NOSOTROS',
    'nosotros_contenido_header_desc' => 'Praesent pulvinar, libero id placerat luctus, turpis purus mattis orci, in tincidunt ligula odio molestie quam. In id dui vitae risus consectetur pretium ut eget sem.',
    'nosotros_contenido_siabuc_es' => 'SIABUC es un software auxiliar en las labores cotidianas de un centro de informaci&oacute;n o biblioteca, ya sea universitaria, p&uacute;blica o particular, sin importar que sea pequeña o grande. El funcionamiento de SIABUC est&aacute; basado en m&oacute;dulos, cada m&oacute;dulo corresponde a una tarea espec&iacute;fica dentro de la biblioteca, los m&oacutedulos',
    'nosotros_contenido_siabuc_es_1' => 'Vivamus facilisis diam quis tristique pretium. Donec mollis ipsum nec volutpat condimentum. Vivamus nec gravida purus, vel fermentum risus. Nulla ac egestas tellus. Suspendisse mauris lacus, venenatis condimentum mauris eget, venenatis elementum leo. Proin facilisis sagittis mauris in pulvinar. Ut fermentum maximus arcu a hendrerit. In consequat leo ut scelerisque fermentum. Phasellus euismod massa erat, eu sollicitudin ipsum faucibus non. Praesent et velit sed nisi tincidunt consequat. Mauris non imperdiet enim, sit amet fringilla justo.',
    'nosotros_contenido_historia' => 'M&aacute;s de 25 AÑOS DE EVOLUCI&oacute;N DE SIABUC',
    'nosotros_contenido_historia_2' => 'SIABUC es un software auxiliar en las labores cotidianas de un centro de informaci&oacute;n o biblioteca, ya sea universitaria, p&uacute;blica o particular, sin importar que sea peque&ntilde;a o grande. El funcionamiento de SIABUC est$aacute; basado en m&oacute;dulos, cada m&oacute;dulo corresponde a una tarea espec$iacute;fica dentro de la biblioteca, los m&oacute;dulos principales de SIABUC son:
En 1983 la Universidad de Colima trabaj&oacute; por primera vez en el desarrollo de los programas del paquete denominado Sistema Integral Automatizado de Bibliotecas de la Universidad de Colima (SIABUC), con la finalidad de aplicar la tecnolog&iacute;a computacional a sus procesos bibliotecarios.
Han pasado m$aacute;s de 25 años desde entonces, durante los cuales el proceso de evoluci&oacute;n de SIABUC no se ha detenido, por el contrario, se ha procurado mantenerlo en constante actualizaci&oacute;n, fundamentalmente porque ya son casi 2500 las instituciones que tanto en M&eacute;xico como en Am&eacute;rica Latina lo manejan cotidianamente en sus bibliotecas. Adem$aacute;s ya contamos con representantes en M&eacute;xico, Costa Rica y Venezuela.',
    'nosotros_contenido_bibliotecas_ucol' => 'Algo m&aacute;s sobre SIABUC 9',
    'nosotros_contenido_bibliotecas_ucol_video_1' => 'En este video se muestra el funcionamiento general de SIABUC 9 y su implementaci&oacute;n ene l sistema de bibliotecas de la Universidad de Colima.',
    'nosotros_contenido_bibliotecas_ucol_video_2' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    'nosotros_contenido_bibliotecas_ucol_siabucxxi' => 'SIABUC Siglo XXI',
    'nosotros_contenido_bibliotecas_ucol_siabucxxi_1' => '1999',
    'nosotros_contenido_bibliotecas_ucol_siabucxxi_2' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'nosotros_contenido_bibliotecas_ucol_siabuc8' => 'SIABUC 8',
    'nosotros_contenido_bibliotecas_ucol_siabuc8_1' => '2002',
    'nosotros_contenido_bibliotecas_ucol_siabuc8_2' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'nosotros_contenido_bibliotecas_ucol_siabuc9' => 'SIABUC 9',
    'nosotros_contenido_bibliotecas_ucol_siabuc9_1' => '2008',
    'nosotros_contenido_bibliotecas_ucol_siabuc9_2' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'nosotros_contenido_bibliotecas_ucol_siabuc10' => 'SIABUC 10',
    'nosotros_contenido_bibliotecas_ucol_siabuc10_1' => '2018',
    'nosotros_contenido_bibliotecas_ucol_siabuc10_2' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'nosotros_contenido_bibliotecas_ucol_boton_aprenda_mas' => 'Aprenda m&aacute;s',

    /*Cadenas del contenido de la secci&oacute;n soporte*/
    'soporte_contenido_header' => 'SOPORTE',
    'soporte_contenido_header_desc' => 'Praesent pulvinar, libero id placerat luctus, turpis purus mattis orci, in tincidunt ligula odio molestie quam. In id dui vitae risus consectetur pretium ut eget sem.',
    'soporte_contenido_calendario' => 'Calendario Universitario',
    'soporte_contenido_calendario_1' => 'El servicio de soporte Telef&oacute;nico y por correo electr&oacute;nico est&aacute; sujeto al mismo calendario universitario. Por ello es muy importante estar al tanto de los dias marcados como suspensi&oacute;n de labores.',
    'soporte_contenido_calendario_2' => 'Periodo Julio 2016 - Agosto 2017',
    'soporte_contenido_calendario_3' => 'Los d6iacute;as de suspension de servicio son: <strong>19 de diciembre de 2016 - 9 de enero de 2017</strong>, <strong>21 de marzo</strong>',
    'soporte_contenido_servico_en_linea' => 'Accede a nuestros servicios de soporte en l&iacute;nea',
    'soporte_contenido_servico_en_linea_1' => 'Gesti&oacute;n de P&oacute;lizas de Servicio',
    'soporte_contenido_servico_en_linea_2' => 'Registro de Licencia',
    'soporte_contenido_servico_en_linea_3' => 'Ticket de Servicio',
    'soporte_contenido_servico_en_linea_4' => '&Uacute;ltimas Actualizaciones',
    'soporte_contenido_servico_en_linea_5' => 'Si requiere de asistencia telef&oacute;nica o por Team Viewer, favor de contactar al equipo de soporte en el n&uacute;mero +52 (312) 316-1121 ext. 111.',

    /*Cadenas de la secci&oacute;n de Comercializaci&oacute;n*/
    'ventas_cotizador_remitente' => 'rmartinez@ucol.mx',
    'ventas_frm_invalido' => 'El dato introducido no es v&aacute;lido.',
    'ventas_contenido_header' => 'COMERCIALIZAC&Oacute;N',
    'ventas_contenido_header_desc' => 'Praesent pulvinar, libero id placerat luctus, turpis purus mattis orci, in tincidunt ligula odio molestie quam. In id dui vitae risus consectetur pretium ut eget sem.',
    'ventas_contenido_listado' => 'Nuestros Productos y Servicios',
    'ventas_contenido_listado_1' => 'Aqu&iacute; podr&acute; encontrar una lista de nuestros productos y servicios con una descripci&oacuten general de los mismos.',
    'ventas_contenido_productos_servicios_s9' => 'SIABUC 9',
    'ventas_contenido_productos_servicios_s9_1' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'ventas_contenido_productos_servicios_cps' => 'Contratacii&oacute;n de P&oacute;lizas de Servicio',
    'ventas_contenido_productos_servicios_cps_1' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'ventas_contenido_productos_servicios_cc' => 'Cursos y Consultor&iacute;as',
    'ventas_contenido_productos_servicios_cc_1' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'ventas_contenido_productos_servicios_cd' => 'Conversi&oacute;n de Datos',
    'ventas_contenido_productos_servicios_cd_1' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'ventas_contenido_productos_servicios_s8' => 'SIABUC 8',
    'ventas_contenido_productos_servicios_s8_1' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
    'ventas_contenido_cotiza_en_linea_4' => 'Aqu&iacute; podr&aacute;s realizar una pre-cotizaci&oacute;n',
    'ventas_contenido_cotiza_en_linea_5' => 'Si requiere de asistencia telef&oacute;nica, favor de contactarnos en el n&uacute;mero +52 (312) 316-1121 ext. 111.',
    'ventas_contenido_cotiza_btn' => 'ABRIR',
    'ventas_contenido_cotizador_titulo' => 'Cotizador en L&iacute;nea',
    'ventas_contenido_contacto_alt' => 'TAMBI&Eacute;N PUEDE CONTACTARNOS A TRAV&Eacute;S DE &Eacute;STE FORMULARIO',
    'ventas_contenido_contacto' => 'SI DESEA ADQUIRIR ALGUNO DE NUESTROS PRODUCTOS Y/O SERVICIOS, CONT&Aacute;CTENOS',
    'ventas_contenido_contacto_1' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
    'ventas_contenido_contacto_teleono' => 'Tel&eacute;fono',
    'ventas_contenido_contacto_fax' => 'Fax',
    'ventas_contenido_contacto_cel' => 'M&oacute;vil',
    'ventas_contenido_contacto_rep' => 'Representante',
    'ventas_contenido_contacto_soporte' => 'Soporte T&eacute;cnico',
    'ventas_contenido_contacto_email' => 'E-Mail',
    'ventas_contenido_contacto_email_servicio' => 'E-Mail Servicios',
    'ventas_contenido_contacto_email_soporte' => 'E-Mail Soporte',
    'ventas_contenido_contacto_dir' => 'Direcci&oacute;n',
    'ventas_contenido_contacto_pais' => 'Pa&iacute;s',
    'ventas_contenido_contacto_sitio' => 'Sitio Web',
    'ventas_contenido_contacto_distribuidor_mex' => 'Distribuidor oficial exclusivo para M&eacute;xico',
    'ventas_contenido_contacto_distribuidor_col' => 'Distribuidor oficial exclusivo para Colombia',
    'ventas_contenido_contacto_distribuidor_rd' => 'Distribuidor oficial exclusivo para Rep&uacute;blica Dominicana',
    'ventas_contenido_contacto_ucol_nombre' => 'Universidad de Colima',
    'ventas_contenido_contacto_ucol_tel' => ' +52 (312) 31-611-21 Ext. 49009',
    'ventas_contenido_contacto_ucol_dir' => ' Av. Gonzalo de Sandoval #444 C.P. 28040, Colima, M&eacute;xico',
    'ventas_contenido_contacto_ucol_email' => ' dgti@ucol.mx',

    'ventas_contenido_contacto_sib_nombre' => 'SIB. Sistemas Integrales para Bibliotecas',
    'ventas_contenido_contacto_sib_rep' => ' Octavio Garc&iacute;a Lizcano',
    'ventas_contenido_contacto_sib_soporte' => ' Ing. Andr&eacute;s Castillo Chaves',
    'ventas_contenido_contacto_sib_dir' => ' Calle 28 No. 85C-30, Cali, Colombia',
    'ventas_contenido_contacto_sib_tel' => ' 052-332 90 64',
    'ventas_contenido_contacto_sib_fax' => ' 052-332 90 64',
    'ventas_contenido_contacto_sib_cel' => ' 310-458 33 89, 310-500 29 61, 301-375 69 97',
    'ventas_contenido_contacto_sib_email_servicio' => ' servicios@sib.com.co',
    'ventas_contenido_contacto_sib_email_soporte' => ' soporte@sib.com.co',

    'ventas_contenido_contacto_mt_nombre' => 'M&T Soluciones Documentales',
    'ventas_contenido_contacto_mt_rep' => ' Ing. Aura Estela Montero Morales',
    'ventas_contenido_contacto_mt_soporte' => ' Ing. Eliezer Figueroa',
    'ventas_contenido_contacto_mt_dir' => ' Calle Mario Garcia Alvarado No.3 Ens. Quisqueya, 
Santo Domingo, Rep. Dominicana.',
    'ventas_contenido_contacto_mt_tel' => ' 809-227-2318',
    'ventas_contenido_contacto_mt_cel' => ' 809-706-231',
    'ventas_contenido_contacto_mt_sitio' => ' http://www.myt.com.do',
    'ventas_contenido_contacto_mt_email_servicio' => ' servicios_siabuc@myt.com.do',
    'ventas_contenido_contacto_mt_email_soporte' => ' soporte_siabuc@myt.com.do',
    'ventas_contenido_email_nombre' => 'Nombre (s)',
    'ventas_contenido_email_apellido' => 'Apellidos (s)',
    'ventas_contenido_email_email' => 'Correo electr&oacute;nico',
    'ventas_contenido_email_tel' => 'N&uacute;mero telef&oacute;nico',
    'ventas_contenido_email_msj' => 'Mensaje',
    'ventas_contenido_email_env' => 'Enviar',

    'ventas_form_nombre' => 'Nombre',
    'ventas_form_email' => 'E-Mail',
    'ventas_form_institucion' => 'Instituci&oacute;n/Empresa',
    'ventas_form_cargo' => 'Cargo',
    'ventas_form_pais' => 'Pa&iacute;s',
    'ventas_form_s9' => 'SIABUC 9 Pro',
    'ventas_form_is9' => 'Instalaci&oacute;n SIABUC 9 Pro',
    'ventas_form_curso_ucol' => 'Curso en UCOL',
    'ventas_form_btn_envio' => 'ENVIAR',
    'ventas_form_ph_nombre' => 'Nombre completo',
    'ventas_form_ph_correo' => 'Escriba su email',
    'ventas_form_ph_institucion' => '¿Donde trabaja?',
    'ventas_form_ph_cargo' => '¿Cu&aacute;l es su cargo?',
    'ventas_form_ph_pais' => '¿De donde se conecta?',
    'ventas_form_envia_mail' => 'Enviar a mi correo electr&oacute;nico',
    'ventas_modal_cotizador_titulo' => 'Este es el costo',
    'ventas_modal_cotizador_cerrar' => 'Cerrar',

    'ventas_modal_cotizador_cargando_msg' => 'Nuestro equipo est&aacute; trabajando.',
    'ventas_modal_cotizador_error_msg' => 'Parece que sucedi&oacute; algo, intente de nuevo porfavor.',
    'ventas_modal_cotizador_incompleto_msg' => 'Es necesario que elijas los productos que deseas adquirir y proporciones todos los datos que se solicitan.',

    'ventas_modal_cotizador_cargando_hdr' => 'Espere un Momento',
    'ventas_modal_cotizador_error_hdr' => 'Oops!',
    'ventas_modal_cotizador_incompleto_hdr' => 'Informaci&oacute;n Incompleta',

    'ventas_mail_estimado' => 'Estimada(o), ',
    'ventas_mail_estimado_2' => 'Estimado &lt;nombre&gt;, agradecemos su inter&eacute;s por nuestro software de automatizaci&oacute;n para bibliotecas, y de acuerdo a su solicitud le hacemos llegar la siguiente pre-cotizaci&oacute;n:',
    'ventas_mail_footer_disc' => 'Tambi&eacute;n consideramos pertinente mencionar que esto se trata de una pre-cotizaci&oacute;n y por lo tanto solo tiene la finalidad proporcionar un monto estimado del costo de nuestro software y servicios. Si desea obtener una cotizaci&oacute;n definitiva adem&aacute;s de incluir otros servicios como cursos de capacitaci&oacute;n (en sitio o en nuestras instalaciones), conversi&oacute;n de datos, desarrollo y otros servicios relacionados con el despliegue y uso diario de nuestro software puede ingresar a nuestra plataforma cotizaci&oacute;n y venta en l&iacute;nea o bien contactar directamente con alguno de nuestros distribuidores.',
    'ventas_mail_footer_btn' => 'Ir',
    'ventas_mail_subject_precotiza' => 'Se envía precotización',
    'ventas_mail_subject_contacta' => 'Contacto SIABUC',
    'ventas_mail_contacta_nombre' => 'Persona que contacta:',
    'ventas_mail_contacta_correo' => 'Email de la persona que contacta:',
    'ventas_mail_contacta_telefono' => 'Tel&eacute;fono de la persona que contacta:',
    'ventas_mail_contacta_mensaje' => 'Motivo por el que contacta:',
    'ventas_mail_contacta_saludo' => 'Hola,',
    'ventas_mail_contacta_intro' => 'Se recibi&oacute; una solicitud en el formulario de contacto de nuestro web; a continuaci&oacute;n se muestra la solicitud:',

    /*Cadenas de la Secci&oacute;n de Documentaci&oacute;n*/
    'doc_contenido_header' => 'Documentaci&oacute;n',
    'doc_contenido_header_desc' => 'Praesent pulvinar, libero id placerat luctus, turpis purus mattis orci, in tincidunt ligula odio molestie quam. In id dui vitae risus consectetur pretium ut eget sem.',
    'doc_contenido_faq_p' => 'Pregunta:',
    'doc_contenido_faq_boton' => 'Respuesta',
    'doc_wiki_header' => 'INGRESA A NUESTRA WIKI',
    'doc_faq_header' => 'PREGUNTAS FRECUENTES',
    'doc_ingresa_wiki' => 'LA WIKI DE SIABUC',
    'doc_modal_cerrar' => 'Cerrar',
];