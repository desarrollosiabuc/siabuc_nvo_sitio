<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="SIABUC">
        <title>{!! trans('cadenas.nav_titulo') !!}</title>

        <!-- reCaptcha -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        
        <!--favicon -->
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" type="text/css" href="{!! asset ('vendor/bootstrap/css/bootstrap.min.css') !!}">
        
        <link rel="stylesheet" type="text/css" href="{!! asset ('css/bootstrapValidator.min.css') !!}">

        <!-- Animate.css -->
        <link rel="stylesheet" type="text/css" href="{!! asset ('css/animate.min.css') !!}">

        <!-- Plugin CSS -->
        <link rel="stylesheet" type="text/css" href="{!! asset ('vendor/font-awesome/css/font-awesome.min.css') !!}">

        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="{!! asset ('css/styles.css') !!}">
        <link rel="stylesheet" type="text/css" href="{!! asset ('css/multipage.css') !!}">
    </head>
    <body>
        @include ('partials.menu')
        @yield('header')
        @yield('content')
        @include ('partials.footer')

        <!-- jQuery -->
        <script type="text/javascript" src="vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrapValidator.min.js"></script>
        <script type="text/javascript" src="js/language/es_ES.js"></script>

        <!-- Wow Animations -->
        <script type="text/javascript" src="js/wow.min.js"></script>

        <!-- Plugin JavaScript -->
        <script type="text/javascript" src="js/jquery.easing.min.js"></script>

        <!-- Theme JavaScript -->
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/jquery.isotope.js"></script>

        <!-- Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-89257603-1', 'auto');
            ga('send', 'pageview');
        </script>
        
        <!-- reCaptcha Script -->
        <!-- <script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script> -->
    </body>
</html>