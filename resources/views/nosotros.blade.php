@extends('layouts.master')
<?php
    $titulo = array(
        'titulo_encabezado' => trans('cadenas.nosotros_contenido_header'),
        'desc_encabezado' => trans('cadenas.nosotros_contenido_header_desc'),
    );
?>
@section('header')
    @include ('partials.headergeneral', array('titulo' => $titulo))
@endsection
@section('content')
	<section class="sec1">
	    <div class="container">
	        <div class="row">
	        	<div class="col-md-4 col-sm-12">
	            	<img src="img/libro_siabuc.jpg" class="img-responsive" alt="abt-img" />
	            </div>
	            <div class="col-md-6 col-sm-12">
	                <p class="vline sub-txt wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_siabuc_es') !!}</p>
	                <p>{!! trans('cadenas.nosotros_contenido_siabuc_es_1') !!}</p>
	            </div>
	        </div>
	    </div>
	</section>
	<section id="services" class="services">
        <div class="container">
            <div class="row">
            	<div class="col-md-6 col-sm-12">
                	<h2 class="section-heading wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_historia') !!}</h2>
                    <p class="sub-txt wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_historia_2') !!}</p>
                </div>
                <div class="col-md-6 col-sm-12">
                	<img src="img/siabuc_quality.png" alt="badge" />
                </div>
            </div>
        </div>
    </section>

    <section id="works" class="works">
        <div class="container">
        	<div class="row">
            	<div class="col-md-6 col-sm-12">
                </div>
            	<div class="col-md-6 col-sm-12">
        			<h2 class="section-heading wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol') !!}</h2>
                </div>
            </div>
            <div class="row">
            	<div class="col-sm-12 col-md-6">
                	<div id="video">
                    	<img src="img/play-icon.png" alt="play-icon" />
                    </div>                	
                </div>
                <div class="col-sm-12 col-md-6">
                    <p class="vline sub-txt wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_video_1') !!}</p>
                    <p>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_video_2') !!}</p>
                </div>
            </div>
            <div class="row mtop">
            	<div class="col-md-3 col-sm-6">
                	<div class="flow">
                    	<div class="row">
                            <div class="col-sm-8 col-xs-7"><h4 class="wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabucxxi') !!}</h4><h5>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabucxxi_1') !!}</h5><hr></div>
                        </div>
                        <p>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabucxxi_2') !!}</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                	<div class="flow">
                    	<div class="row">
                            <div class="col-sm-8 col-xs-7"><h4 class="wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc8') !!}</h4><h5>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc8_1') !!}</h5><hr></div>
                        </div>
                        <p>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc8_2') !!}</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                	<div class="flow">
                    	<div class="row">
                            <div class="col-sm-8 col-xs-7"><h4 class="wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc9') !!}</h4><h5>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc9_1') !!}</h5><hr></div>
                        </div>
                        <p>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc9_2') !!}</p>
                        <a href="<?php echo $app['url']->to('/') . ':81/index.php/siabuc9' ?>" class="page-scroll" target="_blank"><button type="button" class="btn btn-flow hvr-btn wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_boton_aprenda_mas') !!}</button></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                	<div class="flow">
                    	<div class="row">
                            <div class="col-sm-8 col-xs-7"><h4 class="wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc10') !!}</h4><h5>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc10_1') !!}</h5><hr></div>
                        </div>
                        <p>{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_siabuc10_2') !!}</p>
                        <a href="<?php echo $app['url']->to('/') . ':81/index.php/siabuc10' ?>" class="page-scroll" target="_blank"><button type="button" class="btn btn-flow hvr-btn wow fadeInUp animated">{!! trans('cadenas.nosotros_contenido_bibliotecas_ucol_boton_aprenda_mas') !!}</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include ('partials.contacta')
@endsection