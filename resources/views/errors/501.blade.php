@extends('layouts.master')
<?php
    $titulo = array(
        'titulo_encabezado' => trans('cadenas.501_contenido_header'),
        'desc_encabezado' => trans('cadenas.501_contenido_header_desc'),
    );
?>
@section('header')
    @include ('partials.headergeneral', array('titulo' => $titulo))
@endsection
@section('content')

    <section class="sec1">
        <div class="container">
            <div class="row">
                <img src="img/errores/501.png" class="img-responsive" alt="abt-img" />
            </div>
        </div>
    </section>
@endsection