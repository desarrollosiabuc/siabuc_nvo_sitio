@extends('layouts.master')
<?php
    $titulo = array(
        'titulo_encabezado' => trans('cadenas.soporte_contenido_header'),
        'desc_encabezado' => trans('cadenas.soporte_contenido_header_desc'),
    );
?>
@section('header')
    @include ('partials.headergeneral', array('titulo' => $titulo))
@endsection
@section('content')
    <section class="counter">
        <div class="container">
            <div class="row">
                <h2 class="section-heading wow fadeInUp animated">{!! trans('cadenas.soporte_contenido_servico_en_linea') !!}</h2>
                <a href="http://siabuc.ucol.mx/Poliza" target="_blank">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="counter-box wow fadeIn animated">
                            <img src="img/clients.png" alt="counter-icon" />
                            <h5>{!! trans('cadenas.soporte_contenido_servico_en_linea_1') !!}</h5>
                        </div>
                    </div>
                </a>
                <a href="http://siabuc.ucol.mx/site/include/convenios9.php" target="_blank">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="counter-box wow fadeIn animated">
                            <img src="img/projects.png" alt="counter-icon" />
                            <h5>{!! trans('cadenas.soporte_contenido_servico_en_linea_2') !!}</h5>
                        </div>
                    </div>
                </a>
                <a href="http://siabuc.ucol.mx:42422/pro_users/login" target="_blank">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="counter-box wow fadeIn animated">
                            <img src="img/employees.png" alt="counter-icon" />
                            <h5>{!! trans('cadenas.soporte_contenido_servico_en_linea_3') !!}</h5>
                        </div>
                    </div>
                </a>
                <a href="http://siabuc.ucol.mx/actTemp2/include/index.php" target="_blank">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="counter-box wow fadeIn animated">
                            <img src="img/coffee.png" alt="counter-icon" />
                            <h5>{!! trans('cadenas.soporte_contenido_servico_en_linea_4') !!}</h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="container quote">
            <p>{!! trans('cadenas.soporte_contenido_servico_en_linea_5') !!}</p>
        </div>
    </section>

	<section id="services" class="services">
        <div class="container">
            <div class="row">
            	<div class="col-md-12 col-sm-12">
                	<h2 class="section-heading wow fadeInUp animated">{!! trans('cadenas.soporte_contenido_calendario') !!}</h2>
                    <p class="sub-txt wow fadeInUp animated">{!! trans('cadenas.soporte_contenido_calendario_1') !!}</p>
                    <row>
                        <div class="col-md-6 col-sm-12">
                    	   <img src="img/loro_vacaciones.gif" class="img-responsive" alt="people-map" />
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h4>{!! trans('cadenas.soporte_contenido_calendario_2') !!}</h4>
                            <p class="sub-txt wow fadeInUp animated">{!! trans('cadenas.soporte_contenido_calendario_3') !!}</p>
                        </div>
                    </row>
                </div>
            </div>
        </div>
    </section>
	
    @include ('partials.contacta')
@endsection