@extends('layouts.master')
@section('header')
    @include ('partials.headerhome')
@endsection
@section('content')
    <section id="about" class="abt">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h2 class="section-heading wow fadeInUp animated">{!! trans('cadenas.inicio_contenido_saludo') !!}</h2>
                </div>
                <div class="col-sm-8">
                    <p class="vline sub-txt wow fadeInUp animated">{!! trans('cadenas.inicio_contenido_saludo_1') !!}<span>{!! trans('cadenas.inicio_contenido_saludo_2') !!}</span>{!! trans('cadenas.inicio_contenido_saludo_3') !!}</p>
                    <p>{!! trans('cadenas.inicio_contenido_saludo_4') !!}</p>         
                </div>
            </div>
            <div class="row mtop">
                <div class="col-sm-4">
                    <h3 class="section-heading wow fadeInUp animated">{!! trans('cadenas.inicio_contenido_es_usuario') !!}</h3>
                </div>
            </div>
            <div class="row mtop">
                <div class="col-sm-4">
                    <a href="http://siabuc.ucol.mx/actTemp2/include/index.php" target="_blank"><img src="img/abt-5.png" alt="abt-icon" ></a>
                    <h4 class="wow fadeInUp animated">{!! trans('cadenas.inicio_contenido_actualizaciones') !!}</h4>
                    <p>{!! trans('cadenas.inicio_contenido_actualizaciones_2') !!}</p>
                </div>
                <div class="col-sm-4">
                    <a href="http://siabuc.ucol.mx/Poliza" target="_blank"><img src="img/abt-4.png" alt="abt-icon" ></a>
                    <h4 class="wow fadeInUp animated">{!! trans('cadenas.inicio_contenido_poliza') !!}</h4>
                    <p>{!! trans('cadenas.inicio_contenido_poliza_2') !!}</p>
                </div>
                <div class="col-sm-4">
                    <a href="http://siabuc.ucol.mx:42422/pro_users/login" target="_blank"><img src="img/abt-6.png" alt="abt-icon" ></a>
                    <h4 class="wow fadeInUp animated">{!! trans('cadenas.inicio_contenido_ticket') !!}</h4>
                    <p>{!! trans('cadenas.inicio_contenido_ticket_2') !!}</p>
                </div>
            </div>
        </div>
    </section>
    <section id="#testimonials" class="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="section-heading wow fadeInUp animated">{!! trans('cadenas.inicio_contenido_casos_exito') !!}</h1>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <!-- Carousel Slides / Quotes -->
                            <div class="carousel-inner text-center">
                                <!-- Quote 1 -->
                                <div class="item active">
                                    <blockquote>
                                        <div class="row ">
                                            <div class="col-md-2 col-sm-4 col-xs-12">
                                                <img src="img/testimonial-logo-1.png" alt="testimonial" />
                                                <p class="cname">Global-G Solution 1</p>
                                                <p class="cadd">Canada - US</p>
                                            </div>
                                            <div class="col-md-7 col-sm-6 col-xs-12 col-sm-offset-2 box-test">
                                                <p><span>"</span> On-Time project delivery. wonderful quality work!! I visited when my projects was in developing stage there working style & environment is simply superb!!! <span>"</span></p>
                                                <hr>
                                                <h5 class="wow fadeInUp animated">Joseph Lee - C0O</h5>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!-- Quote 2 -->
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-4 col-xs-12">
                                                <img src="img/testimonial-logo-1.png" alt="testimonial" />
                                                <p class="cname">Global-G Solution 2</p>
                                                <p class="cadd">Canada - US</p>
                                            </div>
                                            <div class="col-md-7 col-sm-6 col-xs-12 col-sm-offset-2 box-test">
                                                <p><span>"</span> On-Time project delivery. wonderful quality work!! I visited when my projects was in developing stage there working style & environment is simply superb!!! <span>"</span></p>
                                                <hr>
                                                <h5 class="wow fadeInUp animated">Joseph Lee - C0O</h5>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!-- Quote 3 -->
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-md-2 col-sm-4 col-xs-12">
                                                <img src="img/testimonial-logo-1.png" alt="testimonial" />
                                                <p class="cname">Global-G Solution 3</p>
                                                <p class="cadd">Canada - US</p>
                                            </div>
                                            <div class="col-md-7 col-sm-6 col-xs-12 col-sm-offset-2 box-test">
                                                <p><span>"</span> On-Time project delivery. wonderful quality work!! I visited when my projects was in developing stage there working style & environment is simply superb!!! <span>"</span></p>
                                                <hr>
                                                <h5 class="wow fadeInUp animated">Joseph Lee - C0O</h5>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                            </div>
        
                            <!-- Carousel Buttons Next/Prev -->
                            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="left lspace carousel-control"><i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection