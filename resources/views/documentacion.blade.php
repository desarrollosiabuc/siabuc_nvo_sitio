@extends('layouts.master')
<?php
	$titulo = array(
		'titulo_encabezado' => trans('cadenas.doc_contenido_header'),
		'desc_encabezado' => trans('cadenas.doc_contenido_header_desc'),
	);
?>
@section('header')
    @include ('partials.headergeneral', array('titulo' => $titulo))
    <?php use App\Faq; ?>
@endsection
@section('content')
<section class="counter">
        <div class="container">
            <div class="row">
            	<h3 class="section-heading wow fadeInUp animated">{!! trans('cadenas.doc_faq_header') !!}</h3>
            	<?php
                    $faqs = Faq::all();
                    $aux = 1;
                    foreach ($faqs as $faq) {
                ?>
                	<!-- FAQ -->
                	<div class="col-md-3 col-sm-6 col-xs-6">
	                    <div class="counter-box wow fadeIn animated animated" style="visibility: visible; animation-name: fadeIn;">
	                    	<h5>{!!  trans('cadenas.doc_contenido_faq_p') !!}</h5>
	                    	<div>{!! $faq->faq_esp_mx !!}</div>
	                        <button type="button" class="btn btn-default hvr-btn wow fadeInUp animated animated" style="visibility: visible; animation-name: fadeInUp;" data-toggle="modal" data-target="#modalq<?= $aux ?>">{!!  trans('cadenas.doc_contenido_faq_boton') !!}</button>
	                        
	                    </div>
	                </div>


	                <!-- Modal -->
					<div id="modalq<?= $aux ?>" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<!-- Modal content-->
						    <div class="modal-content">
						    	<div class="modal-header">
						    		<button type="button" class="close" data-dismiss="modal">&times;</button>
						    		<h4 class="modal-title">{!! $faq->faq_esp_mx !!}</h4>
								</div>
								<div class="modal-body">
									<section id="services" class="services">
										<div class="container">
							        		<div class="row">
								        		<div class="col-md-7 col-sm-12">
										        	{!! $faq->r_esp_mx !!}
								        		</div>
							        		</div>
							    		</div>
									</section>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">{!! trans('cadenas.doc_modal_cerrar') !!}</button>
								</div>
							</div>
						</div>
					</div>
                <?php
                        $aux++;
                    }
                ?>
            </div><br><br><br>
            <div class="row">
            	<h3 class="section-heading wow fadeInUp animated">{!! trans('cadenas.doc_wiki_header') !!}</h3>
            	<a href="<?php echo $app['url']->to('/') . ':81/' ?>" target="_blank">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="counter-box wow fadeIn animated">
                            <img src="img/logo.gif" alt="icono-siabuc" class="img-responsive">
                            <h5>{!! trans('cadenas.doc_ingresa_wiki') !!}</h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </section>
	@include ('partials.contacta')
@endsection