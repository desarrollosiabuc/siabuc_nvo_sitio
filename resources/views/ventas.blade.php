@extends('layouts.master')
<?php
    $titulo = array(
        'titulo_encabezado' => trans('cadenas.ventas_contenido_header'),
        'desc_encabezado' => trans('cadenas.ventas_contenido_header_desc'),
    );
?>
@section('header')
    @include ('partials.headergeneral', array('titulo' => $titulo))
    <?php use App\Producto; ?>
@endsection
@section('content')
    <section id="services" class="services">
        <div class="container">
            <div class="row">
                <h2 class="section-heading wow fadeInUp animated">{!! trans('cadenas.ventas_contenido_listado') !!}</h2>
                <p class="sub-txt wow fadeInUp animated">{!! trans('cadenas.ventas_contenido_listado_1') !!}</p>
            </div>
            <div class="row">
                <!-- Aquí inicia el formulario de pre-cotización -->
                <form id="frm_pre_cotizador"
                    method="POST"
                    class="form-horizontal"
                    data-bv-message="{!! trans('cadenas.ventas_frm_invalido') !!}"
                    data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                    data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                    data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">
                    {{ csrf_field() }}
                	<div class="col-md-6 col-sm-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php
                                $productos = Producto::all();
                                $aux = 1;
                                foreach ($productos as $producto) {
                            ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading_{!! $producto->id !!}">
                                        
                                        <input class="input-check" type="checkbox" name="chec_{!! $producto->id !!}">
                                        <h4 class="panel-title">&nbsp;
                                            <a <?= $aux>1 ? 'class="collapsed"' : '' ?> data-toggle="collapse" data-parent="#accordion" href="#collapse_{!! $producto->id !!}" aria-expanded="<?= $aux==1 ? 'true' : 'false' ?>" aria-controls="collapse_{!! $producto->id !!}">{!! $producto->producto !!}</a>
                                        </h4>
                                    </div>
                                    <div id="collapse_{!! $producto->id !!}" class="panel-collapse collapse <?= $aux==1 ? 'in' : '' ?>" role="tabpanel" aria-labelledby="heading_{!! $producto->id !!}">
                                        <div class="panel-body">
                                            <p>{!! $producto->descripcion !!}</p>
                                        </div>
                                    </div>
                                </div>
                            <?php
                                    $aux++;
                                }
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group" >
                            <label for "nombre" class="col-md-3 control-label">{!! trans('cadenas.ventas_form_nombre') !!}</label>
                            <div class="col-md-6">
                                <input type="text"
                                    class="form-control"
                                    name="nombre"
                                    placeholder="{!! trans('cadenas.ventas_form_ph_nombre') !!}"
                                    required
                                    data-bv-notempty="true"
                                    data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}" />
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for "correo" class="col-md-3 control-label">{!! trans('cadenas.ventas_form_email') !!}</label>
                            <div class="col-md-6">
                                <input type="email"
                                    class="form-control"
                                    name="correo"
                                    placeholder="{!! trans('cadenas.ventas_form_ph_correo') !!}"
                                    required
                                    data-bv-emailaddress="true"
                                    data-bv-emailaddress-message="{!! trans('cadenas.form_error_email_equivocado') !!}"
                                    data-bv-notempty="true"
                                    data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}" />
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for "institucion" class="col-md-3 control-label">{!! trans('cadenas.ventas_form_institucion') !!}</label>
                            <div class="col-md-6">
                                <input type="text"
                                    class="form-control"
                                    name="institucion"
                                    placeholder="{!! trans('cadenas.ventas_form_ph_institucion') !!}"
                                    required
                                    data-bv-notempty="true"
                                    data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}" />
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for "cargo" class="col-md-3 control-label">{!! trans('cadenas.ventas_form_cargo') !!}</label>
                            <div class="col-md-6">
                                <input type="text"
                                    class="form-control"
                                    name="cargo"
                                    placeholder="{!! trans('cadenas.ventas_form_ph_cargo') !!}"
                                    required
                                    data-bv-notempty="true"
                                    data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}" />
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for "pais" class="col-md-3 control-label">{!! trans('cadenas.ventas_form_pais') !!}</label>
                            <div class="col-md-6">
                                @include ('partials.paises_select', $elId = array('valor' => 'pais'))
                            </div>
                        </div>

                        <div class="form-group" >
                            <div class="col-md-5">
                                <input type="checkbox" name="envia_mail" checked>
                                <label for="envia_mail">{!! trans('cadenas.ventas_form_envia_mail') !!}</label>
                            </div>
                        </div>

                        <!-- <div class="g-recaptcha" data-sitekey="{!! env('RE_CAP_SITE') !!}"></div> -->

                        <div class="form-group">
                            <div class="col-lg-9 col-lg-offset-3">
                                <button type="button" class="btn btn-primary" id="btn_pre_cotiza" name="btn_pre_cotiza">{!! trans('cadenas.ventas_form_btn_envio') !!}</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 center-block wow fadeInRight animated" id="resultadoCotiza">                          
                    </div>
                </form>
                <!-- Termina el formulario de pre-cotizacion -->
                <!-- Modal -->
                <div id="modalCotiza" class="modal fade" role="dialog"> <!-- Aquí se agrega el contenido del modal -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 quote">
                    <p>{!! trans('cadenas.ventas_contenido_cotiza_en_linea_5') !!}</p><br>
                    <!-- <p><button id="btn-cotiza" name="btn-cotiza" type="button" class="btn btn-primary">{!!  trans('cadenas.ventas_contenido_cotiza_btn') !!}</button></p> -->
                </div>
            </div>
        </div>
    </section>
    <section id="contact" class="contact">
        <div class="container">
            <h3 class="section-heading wow fadeInUp animated">{!! trans('cadenas.ventas_contenido_contacto') !!}</h3>
            <p class="inner-sub wow fadeInUp animated">{!! trans('cadenas.ventas_contenido_contacto_1') !!}</p><br> 

            <div class="row">
                <div class="col-sm-4">
                    <img class="img-responsive" src="img/logoConvenioUcol.png" alt="logo">
                    <h4>{!! trans('cadenas.ventas_contenido_contacto_ucol_nombre') !!}</h4>
                    <p class="sub-txt vline">
                        {!! trans('cadenas.ventas_contenido_contacto_distribuidor_mex') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_dir') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_ucol_dir') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_teleono') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_ucol_tel') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_email') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_ucol_email') !!}<br>
                    </p>
                </div>
                <div class="col-sm-4">
                    <img class="img-responsive" src="img/logoSIB.png" alt="logo"><br>
                    <h4>{!! trans('cadenas.ventas_contenido_contacto_sib_nombre') !!}</h4><br>
                    <p class="sub-txt vline">
                        {!! trans('cadenas.ventas_contenido_contacto_distribuidor_col') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_rep') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_sib_rep') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_soporte') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_sib_soporte') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_dir') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_sib_dir') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_teleono') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_sib_tel') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_fax') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_sib_fax') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_cel') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_sib_cel') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_email_servicio') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_sib_email_servicio') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_email_soporte') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_sib_email_soporte') !!}
                    </p>
                </div>
                <div class="col-sm-4">
                    <img class="img-responsive" src="img/logoMT.png" alt="logo"><br>
                    <h4>{!! trans('cadenas.ventas_contenido_contacto_mt_nombre') !!}</h4><br>
                    <p class="sub-txt vline">
                        {!! trans('cadenas.ventas_contenido_contacto_distribuidor_rd') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_rep') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_mt_rep') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_soporte') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_mt_soporte') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_dir') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_mt_dir') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_teleono') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_mt_tel') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_cel') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_mt_cel') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_sitio') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_mt_sitio') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_email_servicio') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_mt_email_servicio') !!}<br>
                        <span>{!! trans('cadenas.ventas_contenido_contacto_email_soporte') !!}</span>{!! trans('cadenas.ventas_contenido_contacto_mt_email_soporte') !!}
                    </p>
                </div>
            </div>
        <!--</div>
    </section>
    <section id="contacto" class="services">
        <div class="container">-->
            <h3 class="section-heading wow fadeInUp animated">{!! trans('cadenas.ventas_contenido_contacto_alt') !!}</h3>
            <div class="row mtop">
                <div class="col-sm-8 col-sm-offset-2" >
                    <form id="frm_contacto"
                        method="POST"
                        class="form-horizontal"
                        data-bv-message="{!! trans('cadenas.ventas_frm_invalido') !!}"
                        data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                        data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                        data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">
                        {{ csrf_field() }}

                        <div class = "row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input type="text"
                                        class="form-control"
                                        name="nombre"
                                        placeholder="{!! trans('cadenas.ventas_contenido_email_nombre') !!}"
                                        autocomplete="off"
                                        required
                                        data-bv-notempty="true"
                                        data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}" />
                                </div>
                                <div class="col-md-6">
                                    <input type="text"
                                        class="form-control"
                                        name="apellido"
                                        placeholder="{!! trans('cadenas.ventas_contenido_email_apellido') !!}"
                                        autocomplete="off"
                                        required
                                        data-bv-notempty="true"
                                        data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}" />
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input type="email"
                                        class="form-control"
                                        name="email"
                                        placeholder="{!! trans('cadenas.ventas_contenido_email_email') !!}"
                                        autocomplete="off"
                                        required
                                        data-bv-emailaddress="true"
                                        data-bv-emailaddress-message="{!! trans('cadenas.form_error_email_equivocado') !!}"
                                        data-bv-notempty="true"
                                        data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}" />
                                </div>
                                <div class="col-md-6">
                                    <input type="text"
                                        class="form-control"
                                        name="telefono"
                                        placeholder="{!! trans('cadenas.ventas_contenido_email_tel') !!}"
                                        autocomplete="off"
                                        required
                                        data-bv-notempty="true"
                                        data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}" />
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            <div class="form-group" >
                                <div class="col-md-12">
                                    <textarea rows="3"
                                        class="form-control textarea"
                                        name="mensaje"
                                        placeholder="{!! trans('cadenas.ventas_contenido_email_msj') !!}"
                                        autocomplete="off"
                                        required
                                        data-bv-notempty="true"
                                        data-bv-notempty-message="{!! trans('cadenas.form_error_vacio') !!}"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class = "row">
                            <div class="form-group">
                                <div class="col-lg-9 col-lg-offset-3">
                                    <button type="button" class="btn btn-primary" id="btn_contacta" name="btn_contacta">{!! trans('cadenas.ventas_form_btn_envio') !!}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div id="modalContacto" class="modal fade" role="dialog"> <!-- Aquí se agrega el contenido del modal -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection