<header class="header-inicio">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="header-content">
                    <div class="header-content-inner">
                        <img src="img/logo-slice.png" alt="logo-slice" />
                        <h4 class="wow fadeInDown animated">{!! trans('cadenas.inicio_header_titulo') !!}</h4>
                        <h1 class="wow fadeInUp animated">{!! trans('cadenas.inicio_header_titulo_sub') !!}</h1>
                        <p>{!! trans('cadenas.inicio_header_mensaje') !!}</p>
                        <div class="btn-sec wow fadeInUp animated">
                            <a href="<?php echo $app['url']->to('/documentacion') ?>" class="page-scroll"><button type="button" class="btn btn-default hvr-btn">{!! trans('cadenas.inicio_header_conozca_mas') !!}</button></a>
                            <a href="<?php echo $app['url']->to('/ventas#contact') ?>" class="page-scroll"><button type="button" class="btn btn-default btn-custom hvr-btn">{!! trans('cadenas.inicio_header_comprar_siabuc') !!}</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>