<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
    <div class="container">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">{!! trans('cadenas.nav_alternar_navegacion') !!}</span><i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="<?php echo $app['url']->to('/') ?>"><img src="img/logo.png" alt="logo"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo $app['url']->to('/') ?>" class="page-scroll hvr-btn">{!! trans('cadenas.nav_inicio') !!}</a></li>
                
                <li><a href="<?php echo $app['url']->to('/nosotros') ?>" class="page-scrol hvr-btn">{!! trans('cadenas.nav_nosotros') !!}</a></li>
                
                <li><a href="<?php echo $app['url']->to('/soporte') ?>" class="page-scroll hvr-btn">{!! trans('cadenas.nav_soporte') !!}</a></li>
                
                <li><a href="<?php echo $app['url']->to('/ventas') ?>" class="page-scroll hvr-btn">{!! trans('cadenas.nav_comercializacion') !!}</a></li>
                
                <li><a href="<?php echo $app['url']->to('/documentacion') ?>" class="page-scroll hvr-btn">{!! trans('cadenas.nav_documentacion') !!}</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>