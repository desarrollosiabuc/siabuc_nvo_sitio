<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{!!  trans('cadenas.ventas_modal_cotizador_incompleto_hdr') !!}</h4>
      </div>
      <div class="modal-body" style="overflow-x auto"> <!-- Aquí entrará el contenido del modal -->
        <div class="row">
            <p>{!!  trans('cadenas.ventas_modal_cotizador_incompleto_msg') !!}</p>
            <div>
                <img src="img/modal_incompleto.gif" class="img-responsive">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">{!!  trans('cadenas.ventas_modal_cotizador_cerrar') !!}</button>
      </div>
    </div>
</div>