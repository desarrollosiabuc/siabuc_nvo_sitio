<header class="inner header-general">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="header-content">
                    <div class="header-content-inner">
                        <h1 class="wow fadeInUp animated">{!! array_get($titulo, 'titulo_encabezado') !!}</h1>
                        <hr>
                        <p>{!! array_get($titulo, 'desc_encabezado') !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>