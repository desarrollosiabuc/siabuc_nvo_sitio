<section class="btm-sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-xs-12">
                <h3 class="wow fadeInUp animated">{!! trans('cadenas.contactanos_msg') !!}</h3>
            </div>
            <div class="col-sm-3 col-xs-12 center-block"><a href="<?php echo $app['url']->to('/ventas#contact') ?>" class="page-scroll"><button type="button" class="btn btn-default hvr-btn wow fadeInUp animated">{!! trans('cadenas.contactanos_btn') !!}</button></a></div>
        </div>
    </div>      
</section>