<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <a href="<?php echo $app['url']->to('/') ?>"><img src="img/logo.png" class="img-responsive wow fadeInUp animated" alt="logo-siabuc"></a>
                    <a href="http://www.ucol.mx/"><img src="img/logo_ucol.png" class="img-responsive wow fadeInUp animated" alt="logo-ucol"></a>
                    <p>{!! trans('cadenas.footer_dgti') !!}</p><br/>
                    <p><span>{!! trans('cadenas.footer_domicilio') !!} </span>{!! trans('cadenas.footer_domicilio_2') !!}</p>
                    <p><span>{!! trans('cadenas.footer_tel') !!} </span>{!! trans('cadenas.footer_tel_2') !!}</p>
                    <p><span>{!! trans('cadenas.footer_correo') !!} </span> {!! trans('cadenas.footer_correo_2') !!}</p>
                    <p>
                        <h3>{!! trans('cadenas.footer_redes_sociales') !!}</h3><br>
                        <a href="https://www.facebook.com/siabuc" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;<a href="https://twitter.com/@siabuc" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                    </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="map-responsive">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d941.7175045165277!2d-103.70203037076276!3d19.24449099918696!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x84255a9932286def%3A0xc9a2a957dc6e6896!2sAv.+Gonzalo+de+Sandoval+426%2C+Las+V%C3%ADboras%2C+Colima%2C+Col.!5e0!3m2!1ses!2smx!4v1480635836827" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>              
        </div>
    </div>
    <div class="fbottom">
        <div class="container">
            <div class="row">
                <div class="wow fadeInUp animated">
                    <p>{!! trans('cadenas.footer_derechos') !!}</p>
                </div>
            </div>
        </div>
    </div>
</footer>