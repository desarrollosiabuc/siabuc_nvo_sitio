<div class="row">
    <h5 class="modal-title">{!!  trans('cadenas.ventas_modal_cotizador_titulo') !!}</h5>
    <table>
        <thead>
        <tr>
            <th>Producto</th>
            <th>Costo</th>
        </tr>
        </thead>
        <tbody>

    <?php
        foreach ($cotizacion as $key => $value) {
            if (preg_match('/costo_/', $value['dato']))
            {
                $costo_total = $value['valor'];
            }
            else
            {
    ?>
        <tr>
            <td class="tabla_normal"><?= $value['dato'] ?></td>
            <td class="moneda">$&nbsp;{!! number_format($value['valor'], 2) !!}</td>
        </tr>
    <?php
            }
        }
    ?>
        <tr>
            <td class="tot_tabla">Total</td>
            <td class="moneda">$&nbsp;{!! number_format($costo_total, 2) !!}</td>
        </tr>
        </tbody>
    </table>
</div>