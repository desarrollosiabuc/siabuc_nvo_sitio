<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            th, td {
                border-bottom: 1px solid #ddd;
                padding: 15px;
                text-align: left;
            }
        </style>
    </head>

    <body bgcolor="#8d8e90">
        <div>
            <h4>{!! trans('cadenas.ventas_mail_contacta_saludo') !!}</h4>
            <p>{!! trans('cadenas.ventas_mail_contacta_intro') !!}</p>
        </div>
        <div class="row" style="overflow-x:auto;">
            <h5 class="modal-title">{!!  trans('cadenas.ventas_mail_contacta_nombre') !!}</h5>
            <p><?= $nombre . ' ' . $apellido ?></p>
            <h5 class="modal-title">{!!  trans('cadenas.ventas_mail_contacta_correo') !!}</h5>
            <p><?= $email ?></p>
            <h5 class="modal-title">{!!  trans('cadenas.ventas_mail_contacta_telefono') !!}</h5>
            <p><?= $telefono ?></p>
            <h5 class="modal-title">{!!  trans('cadenas.ventas_mail_contacta_mensaje') !!}</h5>
            <p><?= $mensaje ?></p>
        </div>
    </body>
</html>