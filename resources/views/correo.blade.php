<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            th, td {
                border-bottom: 1px solid #ddd;
                padding: 15px;
                text-align: left;
            }
        </style>
    </head>

    <body bgcolor="#8d8e90">
        <div>
            <h4>{!! trans('cadenas.precotizacion_correo_saludo') !!}</h4>
            <p>{!! trans('cadenas.precotizacion_correo_intro') !!}</p>
        </div>
        <div class="row" style="overflow-x:auto;">
            <h5 class="modal-title">{!!  trans('cadenas.ventas_modal_cotizador_titulo') !!}</h5>
            <table>
                <thead>
                <tr>
                    <th>Producto</th>
                    <th>Costo</th>
                </tr>
                </thead>
                <tbody>

            <?php
                foreach ($cotizacion as $key => $value) {
                    if (preg_match('/costo_/', $value['dato']))
                    {
                        $costo_total = $value['valor'];
                    }
                    else
                    {
            ?>
                <tr>
                    <td class="tabla_normal"><?= $value['dato'] ?></td>
                    <td class="moneda">$&nbsp;{!! number_format($value['valor'], 2) !!}</td>
                </tr>
            <?php
                    }
                }
            ?>
                <tr>
                    <td class="tot_tabla">Total</td>
                    <td class="moneda">$&nbsp;{!! number_format($costo_total, 2) !!}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div>
            <p>{!! trans('cadenas.precotizacion_correo_cc_header') !!}</p>
            <ul>
                <li>{!! trans('cadenas.precotizacion_correo_cc_1') !!}</li>
                <li>{!! trans('cadenas.precotizacion_correo_cc_2') !!}</li>
                <li>{!! trans('cadenas.precotizacion_correo_cc_3') !!}</li>
                <li>{!! trans('cadenas.precotizacion_correo_cc_4') !!}</li>
            </ul>
        </div>
        @include ('partials.contacta')
    </body>
</html>