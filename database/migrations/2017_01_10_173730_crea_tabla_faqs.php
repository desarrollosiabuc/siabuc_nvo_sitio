<?php

use App\Faq;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreaTablaFaqs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('faqs');
        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('faq_esp_mx');
            $table->text('faq_eng_usa')->nullable();
            $table->text('r_esp_mx');
            $table->text('r_eng_usa')->nullable();
            $table->timestamps();
        });

        Faq::create([
            'faq_esp_mx' => '¿C&oacutemo puedo obtener una capacitaci&oacute;n sobre el uso de SIABUC en mis instalaciones?',
            'faq_eng_usa' => 'How can I get user training for SIABUC on-site?',
            'r_esp_mx' => '<ul style="margin-top:-5px"><li>Realizar la solicitud del curso por lo menos con 15 días de anticipación.</li><li>Cubrir el costo del curso, además de los viáticos del instructor (transporte, hotel y comidas).</li><li>Contar con el equipo de computo y red para la imparticion del curso.</li><li>Se considera un máximo de 10 participantes por cada instructor contratado.</li><li>Se requiere un proyector multimedia con resolucion minima de 1024x768 pixeles.</li><li>Considerar material de apoyo (opcional): Pizarrón o rotafolio, plumones, gises, borrador, etc.</li><li>Para poder llevar a cabo la realización del curso es necesario contar con el mínimo de participantes (4 personas para los cursos internos y 6 personas para los cursos externos).</li></ul>',
            'r_eng_usa' => '<ul style="margin-top:-5px"><li>Realizar la solicitud del curso por lo menos con 15 días de anticipación.</li><li>Cubrir el costo del curso, además de los viáticos del instructor (transporte, hotel y comidas).</li><li>Contar con el equipo de computo y red para la imparticion del curso.</li><li>Se considera un máximo de 10 participantes por cada instructor contratado.</li><li>Se requiere un proyector multimedia con resolucion minima de 1024x768 pixeles.</li><li>Considerar material de apoyo (opcional): Pizarrón o rotafolio, plumones, gises, borrador, etc.</li><li>Para poder llevar a cabo la realización del curso es necesario contar con el mínimo de participantes (4 personas para los cursos internos y 6 personas para los cursos externos).</li></ul>',
            ]);

        Faq::create([
            'faq_esp_mx' => '¿Cu&aacute;les son los rquerimientos m&iacute;nimos de hardware para instalar SIABUC 9?',
            'faq_eng_usa' => 'What are the minimum hardware requiremets for SIABUC 9?',
            'r_esp_mx' => '<ul><li>S.O. Windows 7</li><li>Disco duro de 500 GB</li><li>Microprocesador de 2 GHz</li></ul>',
            'r_eng_usa' => '<ul><li>S.O. Windows 7</li><li>Disco duro de 500 GB</li><li>Microprocesador de 2 GHz</li></ul>',
            ]);

        Faq::create([
            'faq_esp_mx' => '¿C&oacutemo registro mi licencia SIABUC 9?',
            'faq_eng_usa' => 'How can I register my new SIABUC 9 license?',
            'r_esp_mx' => 'Ingrese a la siguiente p&aacute;gina y siga las instrucciones: <span><a href = "http://siabuc.ucol.mx/site/include/convenios9.php" target = "_blank"> Registro de Licencia </a>',
            'r_eng_usa' => 'Ingrese a la siguiente p&aacute;gina y siga las instrucciones: <span><a href = "http://siabuc.ucol.mx/site/include/convenios9.php" target = "_blank"> Registro de Licencia </a>',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs');
    }
}
