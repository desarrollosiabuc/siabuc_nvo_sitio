<?php

use App\Producto;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('productos');
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('producto');
            $table->double('costo_mxn', 8, 2);
            $table->double('costo_mxn_desc', 8, 2);
            $table->double('costo_usd', 8, 2);
            $table->double('costo_usd_desc', 8, 2);
            $table->text('descripcion')->nullable();
            $table->timestamps();
        });

        Producto::create([
            'producto' => 'Siabuc 9 Profesional V.5',
            'costo_mxn' => 10000.00,
            'costo_mxn_desc' => 7000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            'descripcion' => 'Hasta 5 cleintes; incluye OPAC Básico',
            ]);

        Producto::create([
            'producto' => 'Siabuc 9 Profesional V.10',
            'costo_mxn' => 15000.00,
            'costo_mxn_desc' => 13000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            'descripcion' => 'Hasta 10 cleintes; incluye OPAC Básico',
            ]);

        Producto::create([
            'producto' => 'Siabuc 9 Profesional V.20',
            'costo_mxn' => 20000.00,
            'costo_mxn_desc' => 18000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            'descripcion' => 'Hasta 20 cleintes; incluye OPAC Básico',
            ]);

        Producto::create([
            'producto' => 'Siabuc 9 Profesional V.40',
            'costo_mxn' => 35000.00,
            'costo_mxn_desc' => 30000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            'descripcion' => 'Hasta 40 cleintes; incluye OPAC Básico',
            ]);

        Producto::create([
            'producto' => 'Siabuc 9 Profesional V.60',
            'costo_mxn' => 50000.00,
            'costo_mxn_desc' => 43000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            'descripcion' => 'Hasta 60 cleintes; incluye OPAC Básico',
            ]);

        Producto::create([
            'producto' => 'Siabuc 9 Profesional V.100',
            'costo_mxn' => 95000.00,
            'costo_mxn_desc' => 81000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            'descripcion' => 'Hasta 5 cleintes; incluye OPAC Básico',
            ]);

        Producto::create([
            'producto' => 'OPAC Profesional',
            'costo_mxn' => 5000.00,
            'costo_mxn_desc' => 5000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            ]);

        Producto::create([
            'producto' => 'Póliza de Soporte Anual',
            'costo_mxn' => 5000.00,
            'costo_mxn_desc' => 5000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            ]);

        Producto::create([
            'producto' => 'Hora de Soporte Adicional',
            'costo_mxn' => 800.00,
            'costo_mxn_desc' => 800.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            ]);

        Producto::create([
            'producto' => 'Hora de Soporte',
            'costo_mxn' => 1500.00,
            'costo_mxn_desc' => 1500.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            ]);

        Producto::create([
            'producto' => 'Módulo RFID',
            'costo_mxn' => 20000.00,
            'costo_mxn_desc' => 20000.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            ]);

        Producto::create([
            'producto' => 'Instalación Remota de S9',
            'costo_mxn' => 2500.00,
            'costo_mxn_desc' => 2500.00,
            'costo_usd' => 10000.00,
            'costo_usd_desc' => 7000.00,
            'descripcion' => 'Servidor y 5 clientes',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}