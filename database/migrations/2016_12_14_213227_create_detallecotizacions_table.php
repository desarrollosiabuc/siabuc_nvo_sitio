<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallecotizacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('detalle_cotizaciones');
        Schema::create('detalle_cotizaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idCotizacion')->unsigned();
            $table->integer('idProducto')->unsigned();
            $table->timestamps();
        });

        Schema::table('detalle_cotizaciones', function (Blueprint $table) {
            $table->foreign('idCotizacion')->references('id')->on('cotizaciones');
            $table->foreign('idProducto')->references('id')->on('productos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_cotizaciones');
    }
}
