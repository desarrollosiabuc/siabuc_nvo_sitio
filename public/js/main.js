new WOW().init();

(function($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').on('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 100
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').on(function() {
        $('.navbar-toggle:visible').on();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 50
        }
    });

})(jQuery); // End of use strict


//Video Script

$('#video').on('click', function() {
    $(this).html('<iframe src="https://www.youtube.com/embed/UN19qj5BaMM?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>').css('background', 'none');
});


//Counter Script

$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});


//Latest Works Script

var selectedClass = "";
$(".fil-cat").on('click',function(){ 
	selectedClass = $(this).attr("data-rel"); 
	$("#portfolio").fadeTo(100, 0.1);
	$("#portfolio div").not("."+selectedClass).fadeOut().removeClass('scale-anm');
	setTimeout(function() {
	$("."+selectedClass).fadeIn().addClass('scale-anm');
	$("#portfolio").fadeTo(300, 1);
	}, 300); 	
});

$('.mdl').on('click',function(){
    $('.modal-body').empty();
  	var title = $(this).parent('a').attr("title");
  	$('.modal-title').html(title);
  	$($(this).parents('div').html()).appendTo('.modal-body');
  	$('#myModal').modal({show:true});
});

$('.btn_suma').click(function() {
    cant = $('#'+this.id).val();
    cant ++;
    $('#'+this.id).val(cant);
});

$('.btn_resta').click(function() {
    cant = $('#'+this.id).val();
    if (cant>0) cant --;
    $('#'+this.id).val(cant);
});

$('#btn_pre_cotiza').click(function() {
    $('#frm_pre_cotizador').bootstrapValidator();
    $('#frm_pre_cotizador').data('bootstrapValidator').validate();
    if ($('#frm_pre_cotizador').data('bootstrapValidator').isValid()) {
        $('#modalCotiza').empty();
        $('#modalCotiza').load('/ventas/modalcotiza_cargando');
        $('#modalCotiza').modal('show');
        $.ajax({
            type: 'POST',
            url: '/ventas/cotiza',
            data: $('#frm_pre_cotizador').serialize(),
            success: function (data) {
                console.log(data);
                $('#resultadoCotiza').empty();
                $('#modalCotiza').modal('hide');
                $('#resultadoCotiza').append(data);//Aquí va el código para construir el contenido del modal
            },
            error: function (data) {
                console.log('Error:', data);
                $('#modalCotiza').empty();
                $('#modalCotiza').load('/ventas/modalcotiza_error');
            }
        });
    } else {
        $('#modalCotiza').empty();
        $('#modalCotiza').load('/ventas/modalcotiza_incompleto');
        $('#modalCotiza').modal('show');
    }
});

$('#btn_contacta').click(function() {
    $('#frm_contacto').bootstrapValidator();
    $('#frm_contacto').data('bootstrapValidator').validate();
    if ($('#frm_contacto').data('bootstrapValidator').isValid()) {
        $('#modalContacto').empty();
        $('#modalContacto').load('/ventas/modalcotiza_cargando');
        $('#modalContacto').modal('show');
        $.ajax({
            type: 'POST',
            url: '/ventas/contacta',
            data: $('#frm_contacto').serialize(),
            success: function (data) {
                console.log(data);
                $('#modalContacto').modal('hide');
            },
            error: function (data) {
                console.log('Error:', data);
                $('#modalContacto').empty();
                $('#modalContacto').load('/ventas/modalcotiza_error');
            }
        });
    } else {
        $('#modalContacto').empty();
        $('#modalContacto').load('/ventas/modalcotiza_incompleto');
        $('#modalContacto').modal('show');
    }
});