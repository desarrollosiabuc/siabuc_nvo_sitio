<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Esta sección solo se usará cuando se implemente la aplicacion de Soporte al Proceso Comercial de SIABUC

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/home', 'HomeController@index');*/

Auth::routes();

Route::get('/', function () {
    return view ('inicio');
});

Route::get('/nosotros', function () {
    return view ('nosotros');
});

Route::get('/soporte', function () {
    return view ('soporte');
});

Route::get('/soporte/actualizaciones', function () {
    return view ('nosotros');
});

Route::get('/soporte/polizas', function () {
    return view ('nosotros');
});

Route::get('/soporte/reporte', function () {
    return view ('nosotros');
});

Route::get('/soporte/licencias', function () {
    return view ('nosotros');
});

Route::get('/ventas', function () {
    return view ('ventas');
});

Route::any('/ventas/cotiza', 'CotizaController@index');

Route::any('/ventas/contacta', 'ContactaController@index');

Route::get('/ventas/modalcotiza_error', function () {
    return view ('partials.modal_cotizador_error');
});

Route::get('/ventas/modalcotiza_cargando', function () {
    return view ('partials.modal_cotizador_cargando');
});

Route::get('/ventas/modalcotiza_incompleto', function () {
    return view ('partials.modal_cotizador_incompleto');
});

Route::get('/documentacion', function () {
    return view ('documentacion');
});

Route::get('/documentacion/foro', function () {
    return view ('nosotros');
});

Route::get('/documentacion/wiki', function () {
    return view ('nosotros');
});

Route::get('/documentacion/tutoriales', function () {
    return view ('nosotros');
});